﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CreateTheGreatInYouAPI.Entities;

namespace CreateTheGreatInYouAPI.DataModel
{
    public class CreateTheGreatInYouContext : DbContext
    {
        public DbSet<Mentor> Mentors { get; set; }       
    }
}