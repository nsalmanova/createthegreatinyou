namespace CreateTheGreatInYouAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMentor : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Mentors ON");

            Sql("INSERT INTO Mentors (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, PhoneNumber) VALUES " +
                "(1, 'Natalia', 'Salmanova', '1995-02-22 00:00:00', 'Nat', 'Natalia123!', 'nsalmanova15@gmail.com', '0851234567' )");

            Sql("INSERT INTO Mentors (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, PhoneNumber) VALUES " +
                "(2, 'Mark', 'McCormack', '1982-06-02 00:00:00', 'Mark', 'Mark123!', 'marckmccormack@createthegreatinyou.com', '0857894561' )");
        }
        
        public override void Down()
        {
        }
    }
}
