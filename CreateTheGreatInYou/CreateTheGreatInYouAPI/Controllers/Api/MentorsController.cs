﻿using CreateTheGreatInYouAPI.DataModel;
using CreateTheGreatInYouAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CreateTheGreatInYouAPI.Controllers.Api
{
    public class MentorsController : ApiController
    {
        private CreateTheGreatInYouContext ctgiuContext;

        public MentorsController()
        {
            ctgiuContext = new CreateTheGreatInYouContext();
        }

        // GET Api/Mentors
        public IEnumerable<Mentor> GetMentors()
        {
            return ctgiuContext.Mentors.ToList();
        }

        // Get /Api/Mentor/1
        public Mentor GetMentor(int id)
        {
            var mentor = ctgiuContext.Mentors.SingleOrDefault(m => m.Id == id);

            //return a page with error message that is user friendly
            if (mentor == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return mentor;
        }

        //[HttpGet]
        //public int GetMentorId(int umn)
        //{
        //    var mentor = ctgiuContext.Mentors.SingleOrDefault(m => m.MentorUniqueNumber == umn);

        //    //return a page with error message that is user friendly
        //    if (mentor == null)
        //        throw new HttpResponseException(HttpStatusCode.NotFound);

        //    return mentor.Id;
        //}

        // Get /Api/Mentor/username && password
        [HttpGet]
        public int LoginMentor(string email, string password)
        {
            var mentor = ctgiuContext.Mentors.SingleOrDefault(m => m.Email == email && m.Password == password);

            //return a page with error message that is user friendly
            if (mentor == null)
                return 0;

            return mentor.Id;
        }

        // Post /Api/Mentor
        //[HttpPost]
        //public Mentor AddMentor([FromBody]Mentor mentor)
        //{
        //    if (!ModelState.IsValid)
        //        throw new HttpResponseException(HttpStatusCode.BadRequest);

        //    ctgiuContext.Mentors.Add(mentor);
        //    ctgiuContext.SaveChanges();

        //    return mentor;
        //}

        //// Delete /Api/Mentors/1
        //[HttpDelete]
        //public void DeleteMentor(int id)
        //{
        //    var mentorInDb = ctgiuContext.Mentors.SingleOrDefault(m => m.Id == id);

        //    if (mentorInDb == null)
        //        throw new HttpResponseException(HttpStatusCode.NotFound);

        //    ctgiuContext.Mentors.Remove(mentorInDb);
        //    ctgiuContext.SaveChanges();
        //}
    }
}